<?php
// Definice funkce pro výpočet faktoriálu
function factorial($n) {
    // Podmínka pro zastavení rekurze
    if ($n === 0) {
        return 1;
    } else {
        // Rekurzivní volání funkce s nižším číslem
        return $n * factorial($n - 1);
    }
}

// Volání funkce a vypsání výsledku
echo "Faktoriál čísla 5 je: " . factorial(5);
?>
