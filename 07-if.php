<?php
// Definice proměnné s číslem
$number = 10;

// Podmínka - zjištění, zda je proměnná $number větší než 5
if ($number > 5) {
    // Pokud je podmínka splněna (hodnota proměnné $number je větší než 5), vypište zprávu
    echo "Číslo je větší než 5.";
} else {
    // Pokud podmínka není splněna (hodnota proměnné $number není větší než 5), vypište jinou zprávu
    echo "Číslo je menší nebo rovno 5.";
}
echo "\n";