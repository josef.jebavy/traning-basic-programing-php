<?php
// Definice třídy Car
class Car {
    // Vlastnosti (properties) třídy Car
    public $brand;
    public $model;
    public $year;

    // Konstruktor třídy Car
    public function __construct($brand, $model, $year) {
        $this->brand = $brand;
        $this->model = $model;
        $this->year = $year;
    }

    // Metoda pro získání informací o autě
    public function getInfo() {
        return "Auto: {$this->brand}, Model: {$this->model}, Rok: {$this->year}";
    }
}

// Vytvoření instance (objektu) třídy Car
$car1 = new Car("Škoda", "Octavia", 2020);
$car1 = new Car("Škoda", "Octavia", 2020);

// Volání metody getInfo() pro získání informací o autě
echo $car1->getInfo(); // Vypíše: Auto: Škoda, Model: Octavia, Rok: 2020
echo "\n";
?>
