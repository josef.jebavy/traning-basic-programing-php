<?php
// Definice proměnné s hodnotou
$fruit = "Banán";

// Příkaz switch pro rozhodování na základě hodnoty proměnné
switch ($fruit) {
    case "Jablko":
        echo "Zvolili jste jablko.";
        break;
    case "Hruška":
        echo "Zvolili jste hrušku.";
        break;
    case "Banán":
        echo "Zvolili jste banán.";
        break;
    default:
        echo "Zvolili jste jiný druh ovoce.";
}

echo "\n";