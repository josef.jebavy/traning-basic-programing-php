<?php

// Základní použití funkce substr
$str = "Hello world";
echo $str;
echo "\n";
echo "delka retezce je: ".strlen($str)."\n";


echo  "--------------\n";


// Vytažení podřetězce začínajícího od pozice 6 (počítáno od nuly) a obsahujícího 5 znaků
$substring = substr($str, 6, 5);
// https://www.php.net/manual/en/function.substr.php
// Vypsání získaného podřetězce
echo $substring; // vypíše "world"
echo "\n";


echo  "--------------\n";



// Nahrazení výskytu "world" za "John"
$newStr = str_replace("world", "John", $str);
// https://www.php.net/manual/en/function.str-replace.php
// Vypsání upraveného řetězce
echo $newStr; // vypíše "Hello John"


echo "\n";


echo  "--------------\n";
