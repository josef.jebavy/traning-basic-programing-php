<?php

$length = 10;
for ($i = 0; $i < $length; $i++) {
    echo $i . "\n";
}
echo  "--------------\n";
$i=1;
// while cyklus
while ($i <= 10) {
    // Výpis hodnoty proměnné $i
    echo $i . "\n";

    // Inkrementace hodnoty proměnné $i pro další iteraci cyklu
    $i++;
}

echo  "--------------\n";


$colors = array("Red", "Green", "Blue", "Yellow");
$colors = ["Red", "Green", "Blue", "Yellow"];

// Procházení pole pomocí cyklu foreach
foreach ($colors as $color) {
    // Vypsání hodnoty aktuálního prvku pole
    echo $color . "\n";
}
?>