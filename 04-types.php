<?php
//https://modul-is.cz/blog/3-uvod-do-php-3-dil-datove-typy

//String
$stringA = 'text';
$stringB = "\"escape\"";
echo $stringA; //text
echo $stringB; //"escape"

//
//Integer
$intA = 2;
$intB = -3;
$intC = 0xF;
$intD = 010;
echo $intA; //2
echo $intB; //-3
echo $intC; //15
echo $intD; //8


// Float, Double
$floatA = 1.1;
$floatB = 2.5e2;
$floatC = 5E-3;
echo $floatA; //1.1
echo $floatB; //250

echo $floatC; //0.005
echo "\n";

// Boolean
$boolA = true;
$boolB = false;



//Array
$arrayA = array(10, 20, 30);  //[0 => 10, 1 => 20, 2 => 30]
$arrayB = ["jablko", "hruška"]; //[0 => jablko, 1 => hruška]
$arrayC = ["x" => 1, "y" => 0.5]; //[x => 1, y => 0.5]
var_dump($arrayA);
var_dump($arrayB);
var_dump($arrayC);
echo "\n";

echo $arrayB["0"],"\n";
echo $arrayC["y"],"\n";

echo "------------------------\n";
$promenna=null; //null
var_dump($promenna);
echo "\"$promenna\"";
echo "\n";

echo "------------------------\n";
$promenna = "42";
var_dump($promenna);
$promenna = (int) $promenna;
var_dump($promenna);
