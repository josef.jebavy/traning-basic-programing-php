<?php
// Název souboru, do kterého chceme zapisovat data
$file_name = 'output.txt';

// Otevření souboru pro zápis
$file = fopen($file_name, 'w');

// Pokud se podařilo otevřít soubor
if ($file) {
    // Text, který chceme zapsat do souboru
    $data = "Toto je obsah, který zapisujeme do souboru.\n";

    // Zápis dat do souboru
    fwrite($file, $data);

    // Uzavření souboru
    fclose($file);

    echo "Data byla úspěšně zapsána do souboru '$file_name'.\n";
} else {
    // Pokud se nepodařilo otevřít soubor
    echo "Chyba při otevírání souboru.\n";
}
?>
